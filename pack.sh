#!/bin/bash
# Simple-stupid shell packaging script.

IFS=$'\n'

if [ -z "$1" ]; then
  >&2 echo "Usage: ${BASH_SOURCE[0]} path/to/main.sh > packed_main.sh
This script recognizes the special comments in the shell files:

> # @pack:resource ./relative/path/to/resource
  
  the file(s) matching the path will be extracted into the current 
  directory before the rest of the script executes.

> # @pack:inline
> source ./relative/path/to/script.sh

  inlines the contents of the referenced script into the main script

The result is a bash script that can be executed inline or curl-ed
from the URL.

When the packed script executes, it extracts embedded files before
continuing with the rest of the script. The extracted resource files
are placed in the current directory. The script will not overwrite
the newer files.
"
  exit 1
fi

# Check prerequisites
for tool in which base64 tar realpath dirname ; do
  if [ -z "$(which $tool 2> /dev/null)" ] ; then
    echo "Missing required tool $tool"
    exit 255
  fi
done

MAIN_SCRIPT=$(realpath "$1")
BASE_PATH=$(dirname "${MAIN_SCRIPT}")/
MAIN_SOURCE=""
RESOURCES=""
CR="
"

function embed_script() {
  local input=$(realpath "$1")  
  local base=$(dirname "$input")/
  local lc=1
  while IFS= read -r line || [ -n "$line" ]
  do
    if [[ "$line" =~ ^\#\! ]]; then
      MAIN_SOURCE+=""
    elif [[ "$line" =~ ^\#[[:space:]]*@pack:([a-zA-Z0-9]+)([[:space:]]+(.*))?$ ]]; then
      local pack_command="${BASH_REMATCH[1]}"
      local pack_arg="${BASH_REMATCH[3]}"
      case "$pack_command" in
        resource)
          local file=$(realpath $base/$pack_arg)
          local head=${file:0:${#BASE_PATH}}
          if [ "$head" == "$BASE_PATH" ]; then
            file=${file:${#head}}
          fi
          RESOURCES+="$file$CR"
        ;;
        inline)
          read -r file
          lc=$((lc + 1))
          if [[ "$file" =~ ^[[:space:]]*source[[:space:]]+(.*) ]] ; then
            file="${base}/${BASH_REMATCH[1]}"
            file=$(realpath "$file")
            if [[ -f "$file" ]]; then
              embed_script "$file"
            else 
              >&2 echo "$input($lc): Cannot find script \"$file\" referenced from \"$input\""
              exit 2
            fi
          else
            >&2 echo "$input($lc): \"${pack_command}\" must be followed by \"source path/to/file.sh\""
          fi
        ;;
        *)
          >&2 echo "$input($lc): Unsupported instruction \"${pack_command}\""
          exit 1
        ;;
      esac
    else
      MAIN_SOURCE+="$line$CR"
    fi
    lc=$((lc+1))
  done < "$input"
}

IFS=$'\n'

embed_script "$MAIN_SCRIPT"

cat <<- END 
#!/bin/bash

# Check prerequisites
for tool in which base64 tar ; do
  if [ -z "\$(which \$tool 2> /dev/null)" ]; then
    >&2 echo "Missing required tool \"\$tool\""
    exit 255
  fi
done

# Extract required files
(base64 -d | tar -xz --keep-newer-files ) << ---
END
(tar -C "${BASE_PATH}" -b 1 -cz --files-from - | base64 -b 1024) << END
$RESOURCES
END
cat <<- END
---
$MAIN_SOURCE
END