#!/bin/bash

# @pack:resource quote.txt
# @pack:resource files
# @pack:inline
source ./lib.sh

echo "This is a simple packaged script"
some_function "$(whoami)"

cat ./quote.txt
ls ./files

cat << ---
That's all folks
---