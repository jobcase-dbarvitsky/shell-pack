# Shell packing script

Packs multiple shell files and resources into one shell file, which is suitable for inline use or curl-ing from URL.

> I feel like I need to apologise for the existence of this thing. You can arguably replace it with just `curl https://whatever | tar -xzf`.
> But for some reason doing `curl https://whatever | sh` feels more natural to some people. Needless to say that running shell scripts like
> that is insecure as heck and should generally be avoided.
>
> - Dan

Example:
```bash
$> ./pack.sh ./example/main.sh > ./test/main.sh
```

See also: 
- [example](./example) source
- [result](./test) of the packing the example source
- Run `./pack.sh` without arguments for help
